# beerapp - ukázková aplikace angular + java devstacku #

nasazeno na http://api-angularcz.rhcloud.com/beer-app/

přístupové údaje v aplikaci:

* user/pass - běžný uživatel, může předávat komentáře
* admin/pass - administrátor, může přidávat a editovat piva a pivovary

## Spuštění projektu lokálně: ##

* naklonujte repozitář

### Java část ###
v hlavním adresáři spusťte 
```
mvn clean install
mvn jetty:run-war
```

### Angular část ###

v adresáři beer-app-frontend/src /main/frontend/ spusťte

```
npm install
gulp devel
```

### Gulp tasks ###

**devel**

* spustí aplikaci ve vývojovém módu (server, livereload, watching)

**build**

* zbilduje aplikaci do složky beer-app-frontend/src /main/frontend/build

Testing

```
# karma
npm run test

# protractor

npm run update-webdriver
# pouze před prvním spuštěním protraktoru

npm run protractor
```
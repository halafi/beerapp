(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.errors', ['ui.router'])

      .config(function($stateProvider) {
        $stateProvider
            .state('error', {
              controller: function($state, $previousState, $scope) {
                $scope.data = $state.current.data;

                $scope.goBack = function() {
                  $previousState.go();
                };
              },
              templateUrl: 'app/error/error.html',
              abstract: true
            })

            .state('error.404', {
              data: {
                class: 'error-404',
                title: 'Pivo nebylo nalezeno',
                displayName: 'ERROR 404'
              }
            })

            .state('error.403', {
              data: {
                class: 'error-403',
                title: 'Na tohle nemáš nárok',
                displayName: 'ERROR 403'
              }
            })

            .state('error.500', {
              data: {
                class: 'error-500',
                title: 'Něco se pokazilo',
                displayName: 'ERROR 500'
              }
            });
      });

})();
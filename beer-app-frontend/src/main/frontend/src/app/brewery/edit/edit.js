(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.brewery.edit', [
    'cz.angular.examples.beerApp.common.brewery.breweryForm',
    'cz.angular.bootstrapNotifications'
  ])
      .controller('brewery.EditController', function($previousState, $log, breweryService, brewery, notifications) {
        this.brewery = brewery;

        this.onSave = function(brewery) {

          notifications.addSuccess('Pivovar byl uložen.');
          $log.log(brewery);

          $previousState.go();
        };

        this.onCancel = function() {
          $previousState.go();
        };

        this.onError = function(error) {
          notifications.addError('Došlo k chybě při ukládání pivovaru, zkuste to prosím znovu.');
          $log.log(error);
        };

      });

})();

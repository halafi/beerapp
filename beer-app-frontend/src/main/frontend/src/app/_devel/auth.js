(function() {
  'use strict';

  /* Controllers */

  angular.module('devel.auth', ['cz.angular.auth'])
      .config(function($stateProvider) {
        $stateProvider

            .state('bootstrap', {
              url: '/devel/bootstrap',
              templateUrl: 'app/_devel/bootstrap.html'
            })

            .state('auth', {
              url: '/devel/auth',
              templateUrl: 'app/_devel/auth.html',
              controller: 'AuthController',
              controllerAs: 'auth'
            });
      })

      .controller('AuthController', function($rootScope, $interval, $http, jwtHelper, authService, API_URI, authLoginModal) {

        this.service = authService;

        this.login = function() {
          authService.login('user', 'pass');
        };

        this.loginModal = function() {
          authLoginModal.prepareLoginModal();
        };

        this.wrongLogin = function() {
          authService.login('user', 'wrongpass');
        };

        this.logout = function() {
          authService.logout();
        };

        this.load = function() {
          $http.get(API_URI + '/_devel/secured').then(function(response) {
            this.data = response.data;
          }.bind(this));
        };

        this.loadDenied = function() {
          $http.get(API_URI + '/_devel/denied').then(function(response) {
            this.data = response.data;
          }.bind(this));
        };

        this.setExpireToken = function(expires) {
          return $http.post(API_URI + '/_devel/token-expire', {expires: expires});
        };

        this.actualizeExpires = function() {
          if (!this.service.info.expires) {
            this.expires = '';
            return;
          }

          var expires = Math.round((this.service.info.expires - (new Date().getTime())) / 1000);

          if (expires < 0) {
            expires = 0;
          }
          this.expires = '(' + expires + ')';
        };

        this.renewToken = function() {
          authService.renewToken();
        };


        $rootScope.$on('cz.angular.auth:loginFailed', function() {
          this.isBadLogin = true;
        }.bind(this));

        $rootScope.$on('cz.angular.auth:loginSuccess', function() {
          this.isBadLogin = false;
        }.bind(this));

        $rootScope.$watch(function() {
              return this.service.info.token;
            }.bind(this),
            function(token) {

              if (!token) {
                this.parts = [];
                this.json = [];
                return;
              }

              this.parts = this.parts || [];
              this.json = this.json || [];

              var parts = token.split('.');

              angular.extend(this.parts, parts);

              if (parts.length !== 3) {
                throw new Error('JWT must have 3 parts');
              }

              this.json[0] = JSON.parse(jwtHelper.urlBase64Decode(parts[0]));
              this.json[1] = JSON.parse(jwtHelper.urlBase64Decode(parts[1]));
              this.json[2] = parts[2];
            }.bind(this)
        );

        $interval(this.actualizeExpires.bind(this), 1000);

      });

})();
(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.detail', [
    'cz.angular.examples.beerApp.beer.detail.rating',
    'cz.angular.examples.beerApp.beer.detail.brewery'
  ])
      .controller('beer.DetailController', function(beer) {
        this.beer = beer;
      })

      .controller('beer.SidebarController', function(beer) {
        this.beer = beer;
      });

})();
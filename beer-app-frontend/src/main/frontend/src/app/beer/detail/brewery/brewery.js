(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.detail.brewery',
      [
        'cz.angular.ratingDirective',
        'uiGmapgoogle-maps'
      ])
      .controller('beer.BreweryController', function(brewery) {
        this.brewery = brewery;

        this.map = {
          'center': {
            'latitude': 49.77271161760392,
            'longitude': 15.546111778076241
          },
          'zoom': 7,
          'marker': {
            latitude: brewery.latitude,
            longitude: brewery.longitude
          },
          options : {scrollwheel : false}

        };

      });

})();
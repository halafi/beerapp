(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.detail.rating', [
        'cz.angular.ratingDirective',
        'cz.angular.bootstrapNotifications'
      ]
  )
      .controller('beer.RatingController', function(beer, ratingService, $state, notifications) {
        this.ratings = beer.ratings;

        this.create = function() {
          if (this.form.$invalid) {
            return;
          }

          ratingService.add(beer, this.new)
              .then(function() {
                notifications.addSuccess('Vaše hodnocení bylo přidáno, děkujeme.');
                $state.reload();
              }.bind(this),
              function() {
                notifications.addError('Došlo k chybě při ukládání hodnocení, zkuste to prosím znovu.');
              }.bind(this));
        };
      });

})();
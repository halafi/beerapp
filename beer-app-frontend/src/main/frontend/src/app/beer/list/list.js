(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.list', [
    'cz.angular.examples.beerApp.common.beer.degreeSelect',
    'cz.angular.bootstrapNotifications'
  ])
      .controller('beer.ListController', function(beerSearchFilter, listData, beerService, $scope, notifications) {

        this.queryParams = beerSearchFilter;
        this.paging = {};

        $scope.$watchCollection('list.queryParams', function(newValue, oldValue) {
          if (newValue == oldValue) {
            return;
          }

          this.reload();
        }.bind(this));

        this.resetForm = function() {
          beerSearchFilter.reset();

          this.reload();
        };

        this.remove = function(beer) {
          beer.$delete()
              .then(function() {
                notifications.addSuccess('Pivo bylo smazáno.');
                this.reload();
              }.bind(this))
              .catch(function() {
                notifications.addError('Došlo k chybě při mazání piva.');
              });
        };

        this.reload = function() {
          beerService.query(beerSearchFilter)
              .then(this.actualizeBeers.bind(this));
        };

        this.actualizeBeers = function(data) {
          this.beers = data.beers;
          this.paging = angular.extend(this.paging, data.paging);
        };

        this.actualizeBeers(listData);
      });

})();
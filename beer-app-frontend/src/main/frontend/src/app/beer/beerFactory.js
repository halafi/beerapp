(function() {
  'use strict';

   angular.module('cz.angular.examples.beerApp.beer.services', [])
      .factory('BeerResourceFactory', function($resource, API_URI) {
        return $resource(API_URI + '/beer/:id',
            {'id': '@id'},
            {
              'update': {method: 'PUT'},
              'query' : {method: 'GET', isArray:true, transformResponse : function(data, headersGetter) {
                return JSON.parse(data).content;
              }}
            });
      })

      .factory('beerSearchFilter', function() {
        return {
          page: 1,
          alcoholOperator: '=',

          reset: function() {
            this.page = 1;
            this.alcohol = '';
            this.alcoholOperator = '=';
            this.degree = '';
            this.query = '';
            this.brewery = null;
          },

          getSearchParams: function() {
            var params = {
              page: this.page,
              query: this.query,
              degree: this.degree
            };

            if (this.brewery) {
              params.brewery = this.brewery.id;
            }

            if (this.alcohol) {
              params.alcohol = this.alcohol;
              params.alcoholOperator = this.alcoholOperator;
            }

            return params;
          }

        };
      })

      .service('beerService', function($http, BeerResourceFactory, API_URI, $q) {
        this.newBeer = function() {
          return new BeerResourceFactory();
        };

        this.query = function(filter) {
          return $http.get(API_URI + '/beer', {params: filter.getSearchParams()})
              .then(function(response) {

                var transformedList = response.data.content.map(function(element) {
                  return new BeerResourceFactory(element);
                });

                return {
                  beers: transformedList,
                  paging: {
                    totalItems: response.data.totalElements,
                    perPage: response.data.size
                  }
                };
              });
        };

        this.getBeer = function(id) {
          return BeerResourceFactory.get({id: id}).$promise;
        };
      })

      .service('ratingService', function($http, API_URI) {

        this.add = function(beer, rating) {
          return $http.post(API_URI + '/beer/' + beer.id + '/ratings', rating);
        };
      });

})();
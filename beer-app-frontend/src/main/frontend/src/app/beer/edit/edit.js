(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.beer.edit', [
    'cz.angular.examples.beerApp.brewery.edit.modal',
    'cz.angular.bootstrapNotifications'

  ])
      .controller('beer.EditController', function(beer, $state, $previousState, $modal, notifications) {
        this.beer = angular.copy(beer);

        this.save = function() {
          if (this.form.$invalid) {
            return;
          }

          delete this.beer.ratings;
          this.beer.$save()
              .then(function(beer) {
                notifications.addSuccess('Pivo bylo uloženo.');
                $state.go('beers.beer.detail', {id: this.beer.id}, {reload: true});
              }.bind(this));
        };

        this.cancel = function() {
          $previousState.go();
        };

        this.createNewBrewery = function() {
          var $modalInstance = $modal.open({
            templateUrl: 'app/beer/edit/breweryModal/template.html',
            controller: 'breweryModalController',
            controllerAs: 'modal',
            resolve: {
              brewery: function() {
                return {};
              }
            }
          });

          $modalInstance.result.then(function(brewery) {
            this.beer.brewery = brewery;
          }.bind(this));
        };
      });

})();
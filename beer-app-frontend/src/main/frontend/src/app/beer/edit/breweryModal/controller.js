(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.brewery.edit.modal', [
    'cz.angular.examples.beerApp.common.brewery.breweryForm',
    'cz.angular.bootstrapNotifications'
  ])
      .controller('breweryModalController', function($modalInstance, $log, brewery, notifications) {
        this.brewery = brewery;

        this.onSave = function(brewery) {
          notifications.addSuccess('Pivovar byl přidán.');
          $modalInstance.close(brewery);
        };

        this.onCancel = function() {
          $modalInstance.dismiss('cancel');
        };

        this.onError = function(error) {
          $log.log(error);
          notifications.addError('Došlo k chybě při ukládání pivovaru, zkuste to prosím znovu.');
        };

      });

})();

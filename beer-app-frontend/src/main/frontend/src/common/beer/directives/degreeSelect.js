(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.common.beer.degreeSelect', [
    'cz.angular.examples.beerApp.common.beer.beerOptionsService'
  ])
      .directive('beerDegreeSelect', function(beerOptionsService) {
        return {
          scope: {
            ngModel: '='
          },
          restrict: 'E',
          templateUrl: 'common/beer/directives/degreeSelect.html',

          link: function(scope) {

            beerOptionsService.getOptions()
                .then(function(options) {
                  scope.options = options.degrees;
                });
          }
        };
      });

})();
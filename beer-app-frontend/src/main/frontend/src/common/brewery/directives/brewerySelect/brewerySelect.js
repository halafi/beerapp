(function() {
  'use strict';

  angular.module('cz.angular.examples.beerApp.common.brewery.brewerySelect', [
        'ui.select',
        'cz.angular.examples.beerApp.brewery.services'
      ]
  )
      .directive('brewerySelect', function() {
        var BrewerySelectController = function(breweryService, filterFilter, $attrs) {

          this.isRequired = $attrs.hasOwnProperty('required');

          breweryService.load().then(function(breweries) {
            this.allBreweries = breweries;
          }.bind(this));

          this.refreshBreweries = function(filterString) {
            this.breweries = filterFilter(this.allBreweries, {name: filterString});
          };
        };

        return {
          restrict: 'E',
          templateUrl: 'common/brewery/directives/brewerySelect/brewerySelect.html',
          scope: {
            model: '=model'
          },
          bindToController: true,
          controllerAs: 'select',
          controller: BrewerySelectController
        };
      })

      .config(function(uiSelectConfig) {
        uiSelectConfig.theme = 'bootstrap';
      });

})();
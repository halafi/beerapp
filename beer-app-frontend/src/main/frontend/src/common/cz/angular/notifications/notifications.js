(function() {
  'use strict';

  function NotificationBuilder(notifications, title, text) {

    this.notifications = notifications;

    this.data = {
      title: title,
      text: text
    };
  }

  NotificationBuilder.prototype.show = function() {
    return this.notifications.add_(this.data);
  };

  NotificationBuilder.prototype.info = function() {
    this.data.type = this.notifications.types.info;
    return this;
  };

  NotificationBuilder.prototype.success = function() {
    this.data.type = this.notifications.types.success;
    return this;
  };

  NotificationBuilder.prototype.warning = function() {
    this.data.type = this.notifications.types.warning;
    return this;
  };

  NotificationBuilder.prototype.error = function() {
    this.data.type = this.notifications.types.error;
    return this;
  };

  NotificationBuilder.prototype.timeout = function(timeoutInMiliSeconds) {
    this.data.timeout = timeoutInMiliSeconds;
    return this;
  };

  NotificationBuilder.prototype.persistent = function() {
    delete this.data.timeout;
    return this;
  };

  NotificationBuilder.prototype.button = function(buttonText) {
    this.data.buttonText = buttonText;
    return this;
  };

  var NotificationService = function($q, $timeout) {

    this.notifications = [];

    this.types = {
      success: "success",
      info: "info",
      warning: "warning",
      danger: "danger",
      error: "danger"
    };

    this.build = function(title, text) {
      return new NotificationBuilder(this, title, text);
    };

    this.addInfo = function(title, text) {
      return this.build(title, text).info().show();
    };

    this.addSuccess = function(title, text) {
      return this.build(title, text).success().show();
    };

    this.addWarning = function(title, text) {
      return this.build(title, text).warning().show();
    };

    this.addError = function(title, text) {
      return this.build(title, text).error().show();
    };

    // TODO konfigurovatelné providerem
    this.getDefaultData_ = function(type) {
      switch (type) {
        case this.types.error:
          return {};
        case this.types.success:
        case this.types.warning:
          return {
            timeout: 5000
          };
        default:
          return {
            type: this.types.info,
            timeout: 5000
          };
      }
    };

    this.add_ = function(notificationData) {
      var defaultData = this.getDefaultData_(notificationData.type);

      var notificationObject = angular.extend(defaultData, notificationData);
      this.notifications.unshift(notificationObject);

      if (notificationObject.hasOwnProperty('timeout')) {
        $timeout(this.remove.bind(this, notificationObject), notificationObject.timeout);
      }

      return $q(function(resolve, reject) {
        notificationObject.onHide = function(reason) {
          if (notificationObject.hasOwnProperty('buttonText')) {
            reject(reason);
          } else {
            resolve(notificationObject);
          }
        };

        if (notificationObject.hasOwnProperty('buttonText')) {
          notificationObject.onClick = function() {
            resolve(notificationObject);
          };
        }
      });
    };

    /**
     * @param notificationObject
     */
    this.remove = function(notificationObject) {
      if (this.remove_(notificationObject)) {
        notificationObject.onHide('remove');
      }
    };

    /**
     * @param notificationObject
     * @returns {boolean}
     * @private
     */
    this.remove_ = function(notificationObject) {
      var index = this.notifications.indexOf(notificationObject);
      if (index >= 0) {
        this.notifications.splice(index, 1);
        return true;
      }

      return false;
    };

    /**
     * @param notificationObject
     */
    this.click = function(notificationObject) {
      notificationObject.onClick();
      this.remove_(notificationObject);
    };
  };

  angular.module('cz.angular.notifications.service', [])
      .service("notifications", NotificationService);

})();
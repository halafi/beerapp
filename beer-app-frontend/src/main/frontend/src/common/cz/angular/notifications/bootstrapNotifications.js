(function() {
  'use strict';

  angular.module('cz.angular.bootstrapNotifications', [
    'ui.bootstrap.alert',
    'cz.angular.notifications.service'
  ])
      .directive("notifications", function() {
        return {
          templateUrl: 'common/cz/angular/notifications/bootstrap-notifications.html',
          controllerAs: 'notificationsController',
          controller: function(notifications) {
            this.notificationsSerice = notifications;

            this.close = function(notification) {
              this.notificationsSerice.remove(notification);
            };

            this.buttonClick = function(notification) {
              this.notificationsSerice.click(notification);
            };
          }

        };
      });

})();
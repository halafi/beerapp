(function() {
  'use strict';

  angular.module('cz.angular.auth')
      .service('authLoginModal', function($modal) {

        var path = 'common/cz/angular/auth/';
        this.modalInstance = null;

        this.createModalInstance = function(template) {
          this.modalInstance = $modal.open({
            templateUrl: template || path + 'authModal.html',
            controller: 'AuthModalLoginCtrl',
            controllerAs: 'auth'
          });

          this.modalInstance.result.then(function(data) {
            delete this.modalInstance;

            return data;
          }.bind(this), function() {
            delete this.modalInstance;
          }.bind(this));
        };

        this.prepareLoginModal = function() {
          if (!this.modalInstance) {
            this.createModalInstance(path + 'authModal.html');
          }

          return this.modalInstance.result;
        };

        this.prepareRejectModal = function() {
          if (!this.modalInstance) {
            this.createModalInstance(path + 'rejectModal.html');
          }

          return this.modalInstance.result;
        };

      })
      .controller('AuthModalLoginCtrl', function($scope, $modalInstance, authService) {
        this.isBadLogin = false;

        $scope.$on('cz.angular.auth:loginFailed', function() {
          this.isBadLogin = true;
        }.bind(this));

        this.login = function() {
          authService.login(this.user.name, this.user.password).then(function(auth) {
            $modalInstance.close(auth);
          });
        };

        this.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      });

})();
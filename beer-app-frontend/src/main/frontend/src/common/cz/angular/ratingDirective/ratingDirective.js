(function() {
  'use strict';

  angular.module('cz.angular.ratingDirective', [])
      .directive('beerRating', function() {
        return {
          restrict: 'A',
          templateUrl: 'common/cz/angular/ratingDirective/ratingDirective.html',
          require: '?^averageRating',

          scope: {
            rating: '=beerRating'
          },
          link: function(scope, element, attr, controller) {
            if (controller) {
              controller.addRating(scope.rating);
            }

            scope.hasStar = function(starNumber) {
              return starNumber <= scope.rating;
            };
          }
        };
      });

})();
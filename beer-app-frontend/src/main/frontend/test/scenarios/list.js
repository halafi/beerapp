'use strict';

describe('On beer list', function() {

  beforeEach(function() {
    browser.get('index.html');
  });

  describe('when searching for query=Rebel and degree=11', function() {
    var firstRow;

    beforeEach(function() {
      element(by.model('list.queryParams.query')).sendKeys('Rebel');
      element(by.model('list.queryParams.degree')).element(by.cssContainingText('option', '11')).click();

      browser.waitForAngular();
      firstRow = element.all(by.repeater('beer in list.beers')).get(0);
    });

    it('first row should be Rebel', function() {
      var beerName = firstRow.element(by.binding('beer.name')).getText();
      expect(beerName).toBe("Rebel");
    });

    describe('and clicking on first row detail', function() {
      beforeEach(function() {
        firstRow.element(by.css('.detail')).click();
      });

      it('name should be Rebel', function() {
        var beerName = element(by.binding('detail.beer.name')).getText();
        expect(beerName).toBe("Rebel");
      });
    });


  });
});



package cz.angular.beerapi.utils;

import cz.angular.beerapi.security.TokenAuthenticationService;
import cz.angular.beerapi.security.filter.Credentials;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by vita on 21.12.14.
 */
public class AuthRestTemplateFactory {
    public static final String LOGIN_URI = "http://localhost:9999/api/login";

    public static class TokenInterceptor implements ClientHttpRequestInterceptor {

        private String token;

        public TokenInterceptor(String token) {

            this.token = token;
        }

        @Override
        public ClientHttpResponse intercept(
                HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
                throws IOException {

            HttpHeaders headers = request.getHeaders();
            headers.add(TokenAuthenticationService.AUTH_HEADER_NAME, token);
            return execution.execute(request, body);
        }
    }

    public static RestTemplate getTemplateWithUserLogged() {
        return getTemplate("user", "pass");
    }

    public static RestTemplate getTemplate(String name, String password) {
        RestTemplate template = new TestRestTemplate();
        ResponseEntity<String> response =
                template.postForEntity(LOGIN_URI, new Credentials(name, password), String.class);

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        String header = response.getHeaders().getFirst(TokenAuthenticationService.AUTH_HEADER_NAME);

        interceptors.add(new TokenInterceptor(header));

        template.setInterceptors(interceptors);
        return template;
    }
}

package cz.angular.beerapi.rest;

import cz.angular.beerapi.domain.Beer;
import cz.angular.beerapi.domain.BeerRepository;
import cz.angular.beerapi.domain.brewery.Brewery;
import cz.angular.beerapi.domain.brewery.BreweryFacade;
import cz.angular.beerapi.domain.brewery.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vita on 16.12.14.
 */
@RestController
@RequestMapping(value = "/api")
public class BreweryController {

    @Autowired
    BreweryFacade breweryFacade;

    @Autowired
    BreweryRepository breweryRepository;

    @Autowired
    BeerRepository beerRepository;

    @RequestMapping(value = "/brewery", method = RequestMethod.GET)
    public Iterable<Brewery> getBreweries() {
        return breweryRepository.findAll();
    }

    @RequestMapping(value = "/brewery/{id}", method = RequestMethod.GET)
    public Brewery getBrewery(@PathVariable(value = "id") Long id) {
        return breweryRepository.findOne(id);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/brewery/{id}", method = RequestMethod.POST)
    public Brewery saveBrewery(@PathVariable(value = "id") Long id, @RequestBody Brewery brewery) {
        return breweryFacade.save(brewery);
    }

    @RequestMapping(value = "/brewery/{id}/beers", method = RequestMethod.GET)
    public List<Beer> saveBrewery(@PathVariable(value = "id") Long id) {
        return beerRepository.findAllByBrewery(breweryRepository.findOne(id));
    }

    @RequestMapping(value = "/brewery", method = RequestMethod.POST)
    public Brewery createBrewery(@RequestBody Brewery brewery) {
        return breweryFacade.save(brewery);
    }
}

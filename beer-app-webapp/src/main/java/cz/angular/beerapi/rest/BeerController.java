package cz.angular.beerapi.rest;

import com.mysema.query.types.expr.BooleanExpression;
import cz.angular.beerapi.domain.*;
import cz.angular.beerapi.domain.brewery.BreweryRepository;
import cz.angular.beerapi.rest.viewModel.BeerForSave;
import cz.angular.beerapi.rest.viewModel.BeerOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by vita on 16.12.14.
 */
@RestController
@RequestMapping(value = "/api")
@Transactional
public class BeerController {

    @Autowired
    BeerRepository beerRepository;

    @Autowired
    BreweryRepository breweryRepository;

    @RequestMapping(value = "/beer/options", method = RequestMethod.GET)
    public BeerOptions getOptions() {
        BeerOptions beerOptions = new BeerOptions();
        beerOptions.setDegrees(beerRepository.getDegrees());
        return beerOptions;
    }

    @RequestMapping(value = "/beer/{id}", method = RequestMethod.GET)
    public Beer getBeer(@PathVariable(value = "id") Long id) {
        Beer beer = beerRepository.findOne(id);

        return beer;
    }


    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/beer/{id}", method = RequestMethod.DELETE)
    public void deleteBeer(@PathVariable(value = "id") Long id) {
        beerRepository.delete(id);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/beer/{id}", method = RequestMethod.POST)
    public Beer saveBeer(@PathVariable(value = "id") Long id, @RequestBody BeerForSave beerForSave) {

        Beer beer = beerRepository.findOne(id);
        beer.setName(beerForSave.getName());
        beer.setDegree(beerForSave.getDegree());
        beer.setDescription(beerForSave.getDescription());
        beer.setAlcohol(beerForSave.getAlcohol());
        beer.setBrewery(breweryRepository.findOne(beerForSave.getBreweryId()));

        return beerRepository.save(beer);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/beer/{id}/ratings", method = RequestMethod.POST)
    public Beer addRating(@PathVariable(value = "id") Long id, @RequestBody Rating rating) {
        Beer beer = beerRepository.findOne(id);

        rating.setDate(new Date());
        rating.setBeer(beer);

        beer.getRatings().add(rating);
        return beerRepository.save(beer);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/beer", method = RequestMethod.POST)
    public Beer createBeer(@RequestBody BeerForSave beerForSave) {

        Beer beer = new Beer(
                beerForSave.getName(),
                beerForSave.getDescription(),
                beerForSave.getDegree(),
                beerForSave.getAlcohol(),
                breweryRepository.findOne(beerForSave.getBreweryId()));

        return beerRepository.save(beer);
    }

    @RequestMapping(value = "/beer", method = RequestMethod.GET)
    public Page<Beer> getBeersPages(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "number", defaultValue = "20") int number,
                                    @RequestParam(value = "query", defaultValue = "") String query,
                                    @RequestParam(value = "degree", defaultValue = "") String degree,
                                    @RequestParam(value = "brewery", defaultValue = "") Long brewery,
                                    @RequestParam(value = "alcohol", defaultValue = "") BigDecimal alcohol,
                                    @RequestParam(value = "alcoholOperator", defaultValue = "=") String alcoholOperator
    ) {

        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "name").ignoreCase();
        PageRequest pageable = new PageRequest(page - 1, number, new Sort(order));

        QBeer qbeer = QBeer.beer;
        BooleanExpression expression = qbeer.name.containsIgnoreCase(query);

        if (brewery != null) {
            expression = expression.and(qbeer.brewery.id.eq(brewery));
        }

        if (!StringUtils.isEmpty(degree)) {
            expression = expression.and(qbeer.degree.eq(degree));
        }

        if (alcohol != null) {
            BooleanExpression alcoholExpression;

            if (alcoholOperator.equals("<")) {
                alcoholExpression = qbeer.alcohol.lt(alcohol);
            } else if (alcoholOperator.equals(">")) {
                alcoholExpression = qbeer.alcohol.gt(alcohol);
            } else {
                alcoholExpression = qbeer.alcohol.eq(alcohol);
            }
            expression = expression.and(alcoholExpression);
        }

        return beerRepository.findAll(expression, pageable);
    }
}

package cz.angular.beerapi.rest.viewModel;

import java.util.List;

/**
 * Created by vita on 28.02.15.
 */
public class BeerOptions {
    private Iterable<String> degrees;

    public Iterable<String> getDegrees() {
        return degrees;
    }

    public void setDegrees(Iterable<String> degrees) {
        this.degrees = degrees;
    }
}

package cz.angular.beerapi.security.filter;

import cz.angular.beerapi.security.TokenAuthenticationService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter for transparent authentication which can go with all REST requests
 *
 * Token is read from custom header
 */
public class StatelessAuthenticationFilter extends GenericFilterBean {
    private TokenAuthenticationService tokenAuthenticationService;

    public StatelessAuthenticationFilter(TokenAuthenticationService tokenAuthenticationService) {
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
            ServletException {

        // read authentication object from header
        Authentication authentication = tokenAuthenticationService.getAuthentication((HttpServletRequest) req);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(req, res);
    }
}

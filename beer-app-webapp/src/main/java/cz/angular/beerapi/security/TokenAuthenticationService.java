package cz.angular.beerapi.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Service for manipulation with token.
 */
@Service
public class TokenAuthenticationService {

	public static final String AUTH_HEADER_NAME = "X-Auth-Token";
	private static final long VALIDITY = 1000 * 60 * 60 * 24 * 10;
	private String secret;
    private int expires;


    @Autowired
	public TokenAuthenticationService(@Value("${token.secret}") String secret, @Value("${token.expires}") int expires) {

		this.secret = secret;
        this.expires = expires;
    }

    public void setExpires(int expires) {
        this.expires = expires;
    }

    /**
     * Adds authentication header to the response
     *
     * Calculates JWT token from user information and adds expiration limit
     *
     * @param response
     * @param authentication
     */
    public void addAuthenticationHeader(HttpServletResponse response, Authentication authentication) {

        UserClaims userClaims = new UserClaims(authentication, expires);
        Jwt token = JwtHelper.encode(toJSON(userClaims), getSigner());
		response.addHeader(AUTH_HEADER_NAME, token.getEncoded());
	}

	private MacSigner getSigner() {
		return new MacSigner(secret);
	}

	private String toJSON(UserClaims authentication) {
		try {
			return new ObjectMapper().writeValueAsString(authentication);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException(e);
		}
	}

    /**
     * Read authentication header and parse JWT token from it.
     * Checks if token is not expired
     *
     * @param request
     * @return User which is being authenticated or NULL
     */
	public Authentication getAuthentication(HttpServletRequest request) {

		final String token = request.getHeader(AUTH_HEADER_NAME);

		if (token == null) {
			return null;
		}

		Jwt jwt = JwtHelper.decodeAndVerify(token, getSigner());
		String json = jwt.getClaims();
		UserClaims userClaims = fromJSON(json);

        long timestamp = new Date().getTime() / 1000;
        if(userClaims.getExp() <= timestamp) {
            return null;
        }

		return new UserAuthentication(userClaims);
	}

	private UserClaims fromJSON(String json) {
		try {
			return new ObjectMapper().readValue(json, UserClaims.class);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
}

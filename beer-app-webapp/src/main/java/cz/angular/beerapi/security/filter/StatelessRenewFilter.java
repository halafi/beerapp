package cz.angular.beerapi.security.filter;

import cz.angular.beerapi.security.TokenAuthenticationService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for renewing token
 *
 * If token is still valid, this filter returns fresh new generated token
 */
public class StatelessRenewFilter extends GenericFilterBean {

    private final AntPathRequestMatcher antPathRequestMatcher;
    private TokenAuthenticationService tokenAuthenticationService;

    public StatelessRenewFilter(String urlMapping, TokenAuthenticationService tokenAuthenticationService) {
        antPathRequestMatcher = new AntPathRequestMatcher(urlMapping);
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        if(antPathRequestMatcher.matches((HttpServletRequest) req)) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            // add token to the header of response
            tokenAuthenticationService.addAuthenticationHeader((HttpServletResponse) res, authentication);
            return;
        }

        chain.doFilter(req, res); // always continue
    }
}
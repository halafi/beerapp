package cz.angular.beerapi.domain.brewery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by vita on 15.12.14.
 */
@Entity
public class Brewery {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private int year;

    @Column
    private String city;

    @Column(precision=18, scale = 12)
    private BigDecimal latitude;

    @Column(precision=18, scale = 12)
    private BigDecimal longitude;

    public Brewery() {
    }

    public Brewery(String name) {
        this.name = name;
    }

    public Brewery(String name, int year, String city) {
        this.name = name;
        this.year = year;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
}

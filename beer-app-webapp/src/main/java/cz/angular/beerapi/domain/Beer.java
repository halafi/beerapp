package cz.angular.beerapi.domain;

import cz.angular.beerapi.domain.brewery.Brewery;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by vita on 15.12.14.
 */
@Entity
public class Beer {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String degree;

    @Column
    private BigDecimal alcohol;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "beer", fetch = FetchType.EAGER)
    private List<Rating> ratings;

    @ManyToOne(optional = true)
    private Brewery brewery;

    public Beer() {

    }

    public Beer(String name, String description, String degree, BigDecimal alcohol, Brewery brewery) {
        this.name = name;
        this.description = description;
        this.degree = degree;
        this.brewery = brewery;
        this.alcohol = alcohol;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public BigDecimal getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(BigDecimal alcohol) {
        this.alcohol = alcohol;
    }
}

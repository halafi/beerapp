package cz.angular.beerapi.domain.brewery;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.LatLng;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * Created by vita on 20.02.15.
 */
@Component
public class BreweryFacade {

    private static Logger logger = Logger.getLogger(BreweryFacade.class);

    @Autowired
    BreweryRepository breweryRepository;

    public Brewery save(Brewery brewery) {

        geocodeLocation(brewery);

        return breweryRepository.save(brewery);
    }

    private void geocodeLocation(Brewery brewery) {

        if (StringUtils.isEmpty(brewery.getCity())) {
            return;
        }

        tryToLoadLocation(brewery, 3);
    }

    private void tryToLoadLocation(Brewery brewery, int count) {

        final Geocoder geocoder = new Geocoder();
        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(brewery.getCity()).getGeocoderRequest();

        try {
            GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);

            LatLng location = geocoderResponse.getResults().get(0).getGeometry().getLocation();
            brewery.setLatitude(location.getLat());
            brewery.setLongitude(location.getLng());
        } catch (Exception ex) {
            if (count > 0) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                logger.warn("Problem with loading location - " + brewery.getCity() + " - try " + count);

                tryToLoadLocation(brewery, count - 1);
            } else {
                logger.error("Problem with loading location - " + brewery.getCity() + " - rejected");
            }
        }
    }
}

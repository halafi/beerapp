package cz.angular.beerapi.domain;

import cz.angular.beerapi.domain.brewery.Brewery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by vita on 15.12.14.
 */
public interface BeerRepository extends CrudRepository<Beer, Long>, QueryDslPredicateExecutor {

  Page<Beer> findAll(Pageable pageable);

  List<Beer> findAllByBrewery(Brewery brewery);

  Page<Beer> findAllByNameContainingIgnoreCase(String name, Pageable pageable);

    @Query("SELECT DISTINCT b.degree FROM Beer b ORDER BY b.degree")
    Iterable<String> getDegrees();
}